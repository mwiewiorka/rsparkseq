require(rJava)
require(jvmr)
require(multicore)

setClass("RSparkSeqAnalysis",
         representation(rsparkcontext = "RSparkContext",
                        sparkseqAnalysis = "jobjRef",
                        regionHashMap = "jobjRef"
                        )               
)
         
RSparkSeqAnalysis <-function(rsparkcontext, bamfile, sampleID, normFactor,reduceWorkers=8,debug=FALSE){
    
  rSparkSeqAnalysis <- new("RSparkSeqAnalysis", rsparkcontext=rsparkcontext)
  rSparkSeqAnalysis@rsparkcontext = rsparkcontext
  interpret(rSparkSeqAnalysis@rsparkcontext@scalaREPL,"rBAMFile",echo.output=debug) <-as.character(bamfile)
  interpret(rSparkSeqAnalysis@rsparkcontext@scalaREPL,"rSampeID",echo.output=debug) <-as.integer(sampleID)
  interpret(rSparkSeqAnalysis@rsparkcontext@scalaREPL,"rNormFactor",echo.output=debug) <-as.numeric(normFactor)
  interpret(rSparkSeqAnalysis@rsparkcontext@scalaREPL,"rReduceWorkers",echo.output=debug) <-as.integer(reduceWorkers)
  interpret(rSparkSeqAnalysis@rsparkcontext@scalaREPL,"rSparkContext",echo.output=debug) <-rSparkSeqAnalysis@rsparkcontext@context
   
  rSparkSeqAnalysis@sparkseqAnalysis <- interpret(rSparkSeqAnalysis@rsparkcontext@scalaREPL,
                                'val rSeqAnalysis = new RSparkSeqAnalysis(rSparkContext,rBAMFile,rSampeID,rNormFactor,rReduceWorkers)'
                                 ,echo.output=debug)
  
  return (rSparkSeqAnalysis)
}
setGeneric("regionHashMap<-", function(x, value) standardGeneric("regionHashMap<-"))
setReplaceMethod("regionHashMap", "RSparkSeqAnalysis",
      function(x, value) {
        interpret(x@rsparkcontext@scalaREPL,"rBEDFilePath",echo.output=TRUE) <-as.character(value)
        interpret(x@rsparkcontext@scalaREPL,"rSeqAnalysis.setRegionMap(sc,rBEDFilePath)",echo.output=TRUE)
        #x@regionHashMap <- ; 
        validObject(x); x}
      ) 

setGeneric("addBAMFile", function(x, value) standardGeneric("addBAMFile"))
setMethod("addBAMFile", "RSparkSeqAnalysis",
                 function(x, value) {
                   interpret(x@rsparkcontext@scalaREPL,"rBAMFilePath",echo.output=TRUE) <-as.character(value[1])
                   interpret(x@rsparkcontext@scalaREPL,"rSampleID",echo.output=TRUE) <-as.integer(value[2])                                                                                     
                   interpret(x@rsparkcontext@scalaREPL,"rSeqAnalysis.addBAM(sc,rBAMFilePath,rSampleID,1.0)",echo.output=TRUE)
                   #x@regionHashMap <- ; 
                   #validObject(x); x
                 }
) 


setGeneric("geneCounts", function(x) standardGeneric("geneCounts"))
setMethod("geneCounts", "RSparkSeqAnalysis", 
      function(x){ 
        featureCountArray <- .jevalArray(x@sparkseqAnalysis$getRGeneCoverage(),simplify=TRUE )
        countNotNULL = 0
        for(i in 1:length(featureCountArray)) {if(!is.jnull( featureCountArray[[i]] ) ) countNotNULL=countNotNULL+1 else break}
        print("Conversion done...")
        featureNumber <-  countNotNULL 
        sampleNumber <-x@sparkseqAnalysis$sampleNum()
        samplesID <- x@sparkseqAnalysis$getSamplesID()
        header<-c("Feature")
        for (i in samplesID) header<-cbind(header,paste("Sample_",i,sep=""))
        #geneCountDF <-data.frame(matrix(vector(), geneNumber,sampleNumber+1 , dimnames=list(c(), header)), stringsAsFactors=F)
        gConv<-mclapply(featureCountArray[1:featureNumber], function(x) c(x$"_1",as.numeric(x$"_2")) )
        featureCountDF <-data.frame(matrix(unlist(gConv), nrow=featureNumber, byrow=T) )
        colnames(featureCountDF) <-header
        return(featureCountDF)
      }
)


setGeneric("exonCounts", function(x) standardGeneric("exonCounts"))
setMethod("exonCounts", "RSparkSeqAnalysis", 
          function(x){ 
            featureCountArray <- .jevalArray(x@sparkseqAnalysis$getRExonCoverage(),simplify=TRUE )
            countNotNULL = 0
            for(i in 1:length(featureCountArray)) {if(!is.jnull( featureCountArray[[i]] ) ) countNotNULL=countNotNULL+1 else break}
            print("Conversion done...")
            featureNumber <-  countNotNULL 
            sampleNumber <-x@sparkseqAnalysis$sampleNum()
            samplesID <- x@sparkseqAnalysis$getSamplesID()
            header<-c("Feature")
            for (i in samplesID) header<-cbind(header,paste("Sample_",i,sep=""))
            #geneCountDF <-data.frame(matrix(vector(), geneNumber,sampleNumber+1 , dimnames=list(c(), header)), stringsAsFactors=F)
            gConv<-mclapply(featureCountArray[1:featureNumber], function(x) c(x$"_1",as.numeric(x$"_2")) )
            featureCountDF <-data.frame(matrix(unlist(gConv), nrow=featureNumber, byrow=T) )
            colnames(featureCountDF) <-header
            return(featureCountDF)
          }
)