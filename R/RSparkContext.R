RSparkContext <- function(master, executor, sparkJar, sparkMesosCoarse=TRUE, debug=FALSE){

    rSparkContext <- new("RSparkContext", master=master, sparkJar=sparkJar, executor=executor, 
                         sparkMesosCoarse=sparkMesosCoarse, debug=debug)
    rSparkContext@scalaREPL <-scalaInterpreter(use.jvmr.class.path=TRUE)
    
    #pass variables to interpreter
    interpret(rSparkContext@scalaREPL,"rSparkMaster",echo.output=debug) <-rSparkContext@master
    interpret(rSparkContext@scalaREPL,"rSparkExecutor",echo.output=debug) <-rSparkContext@executor
    interpret(rSparkContext@scalaREPL,"rSparkJar",echo.output=debug) <-paste(JVMR_JARS_PATH, rSparkContext@sparkJar,sep='/')
    if(sparkMesosCoarse == TRUE)
      interpret(rSparkContext@scalaREPL,'val rSparkMesosCoarse:String="true"',echo.output=debug)
    else
      interpret(rSparkContext@scalaREPL,'val rSparkMesosCoarse:String="false"',echo.output=debug)
    
   interpret(rSparkContext@scalaREPL,'
      import pl.elka.pw.sparkseq.rsparkseq.RSparkSeqAnalysis
      import pl.elka.pw.sparkseq.serialization.SparkSeqKryoProperties
      import org.apache.spark.SparkContext
      import org.apache.spark.SparkContext._
      import org.apache.spark.SparkConf
      import pl.elka.pw.sparkseq.conversions.SparkSeqConversions
      import pl.elka.pw.sparkseq.util.SparkSeqContexProperties'
              ,echo.output=debug)
   
   rSparkContext@context<-interpret(rSparkContext@scalaREPL,'
      SparkSeqContexProperties.setupContexProperties()
      SparkSeqKryoProperties.setupKryoContextProperties()
      val conf = new SparkConf()
        .setMaster(rSparkMaster)
        .setAppName("RSparkSeq")
       .set("spark.executor.uri", rSparkExecutor)
        .setJars(Seq(rSparkJar))
        .setSparkHome(System.getenv("SPARK_HOME"))
       .set("spark.mesos.coarse", rSparkMesosCoarse)
      val sc = new SparkContext(conf)'
                 ,echo.output=debug)
    return(rSparkContext)
  
}